package me.backstabber.epicsetclans.injection;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;

import me.backstabber.epicsetclans.EpicSetClans;
import me.backstabber.epicsetclans.clanhandles.data.DuelArenaData;
import me.backstabber.epicsetclans.clanhandles.manager.ClanDuelManager;
import me.backstabber.epicsetclans.clanhandles.manager.ClanTopManager;
import me.backstabber.epicsetclans.clanhandles.manager.CostsManager;
import me.backstabber.epicsetclans.clanhandles.manager.EpicClanManager;
import me.backstabber.epicsetclans.clanhandles.saving.LocalSaving;
import me.backstabber.epicsetclans.clanhandles.saving.MySqlSaving;
import me.backstabber.epicsetclans.clanhandles.saving.Saving;
import me.backstabber.epicsetclans.clanhandles.saving.SavingManager;
import me.backstabber.epicsetclans.commands.subcommands.admin.ArenaCommand;
import me.backstabber.epicsetclans.hooks.worldguard.WorldguardHook;
import me.backstabber.epicsetclans.utils.AntiKillFarm;

public class BinderModule extends AbstractModule
{
	 private EpicSetClans plugin;
	 private WorldguardHook wgHook;
	 private Injector injector;
	 public BinderModule(EpicSetClans plugin, WorldguardHook wgHook) 
	 {
	        this.plugin = plugin;
	        this.wgHook=wgHook;
			injector=Guice.createInjector(this);
	 }
	 public void injectMembers(Object instance) 
	 {
        injector.injectMembers(instance);
	 }

	 @Override
	 protected void configure() 
	 {
        EpicClanManager clanManager=new EpicClanManager();
        ClanDuelManager duelManager=new ClanDuelManager();
        ClanTopManager topManager=new ClanTopManager();
        this.bind(EpicSetClans.class).toInstance(plugin);
        this.bind(EpicClanManager.class).toInstance(clanManager);
        this.bind(ClanTopManager.class).toInstance(topManager);
        this.bind(ClanDuelManager.class).toInstance(duelManager);
        this.bind(WorldguardHook.class).toInstance(wgHook);
        this.bind(ArenaCommand.class).toInstance(new ArenaCommand(plugin, clanManager, duelManager));
        this.bind(DuelArenaData.class).toInstance(new DuelArenaData());
        this.bind(CostsManager.class).toInstance(new CostsManager());
        this.bind(AntiKillFarm.class).toInstance(new AntiKillFarm());
        this.bind(SavingManager.class).toInstance(new SavingManager());
        if(!plugin.getSettings().getBoolean("mysql.enabled")) {
			this.bind(Saving.class).toInstance(new LocalSaving());
		}
		else  {
			this.bind(Saving.class).toInstance(new MySqlSaving());
		}
	 }
}
