package me.backstabber.epicsetclans.hooks;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

import com.google.inject.Inject;

import me.backstabber.epicsetclans.EpicSetClans;
import me.backstabber.epicsetclans.clanhandles.manager.EpicClanManager;
import me.backstabber.epicsetclans.utils.CommonUtils;
import me.clip.deluxechat.events.DeluxeChatEvent;

public class DeluxeChatHook implements Listener 
{
	@Inject
	private EpicSetClans plugin;
	@Inject
	private EpicClanManager clanManager;
	public static boolean hooked=false;
	public DeluxeChatHook()
	{
		ConsoleCommandSender console = Bukkit.getConsoleSender();
		console.sendMessage(CommonUtils.chat("&a[EpicSet-Clans] >&eFound DeluxeChat. &rHooking into it."));
		hooked=true;
	}
	public static boolean isHooked()
	{
		return hooked;
	}
	@EventHandler
	public void onDeluxeChat(DeluxeChatEvent event)
	{
		Player player=event.getPlayer();
		String msg=event.getChatMessage();
		String id=plugin.getSettings().getString("formats.clan-chat-char");
		if(player.hasMetadata("EpicSetClanChat"))
		{
			event.setCancelled(true);
			String type=player.getMetadata("EpicSetClanChat").get(0).asString();
			if(type==null)
				return;
			if(type.equalsIgnoreCase("clan"))
				plugin.getChatHandle().sendClanChat(player, msg);
			if(type.equalsIgnoreCase("ally"))
				plugin.getChatHandle().sendAllyChat(player, msg);
			if(type.equalsIgnoreCase("truce"))
				plugin.getChatHandle().sendTruceChat(player, msg);
		}
		else if(id!=null&&id.length()>0&&msg.trim().startsWith(id.substring(0,1)))
		{
			if(!clanManager.isInClan(player.getName()))
				return;
			msg=msg.replaceFirst(id.substring(0,1), "");
			event.setCancelled(true);
			plugin.getChatHandle().sendClanChat(player, msg);
		}
	}
}
